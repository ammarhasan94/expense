/**
 * Created by ammar on 9/9/15.
 */

routerApp.factory('Expense',['$http', function($http) {
    var url="http://localhost:3000/expense";

    return {
        getExpenses:function(){
            return $http.get(url);

        },
        postExpense:function(expense){
            console.log(expense);
            return $http.post(url,expense)
        },
        updateExpense:function(expense){
            return $http.put(url ,expense)
        },
        deleteExpense:function(id){
            console.log(id);
            return $http.delete(url+'/'+id)
        }
    };
}]);