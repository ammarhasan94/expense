/**
 * Created by ammar on 9/9/15.
 */

routerApp.factory('Head',['$http', function($http) {
    var url="http://localhost:3000/head";

    return {
        getHeads:function(){
            return $http.get(url);

        },
        getHeadById:function(id){
            return $http.get(url+'/'+id)
        },
        addHead:function(head){
            return $http.post(url,head)
        },
        updateHead:function(head){
            return $http.put(url ,head)
        },
        deleteHead:function(id){
            console.log(id);
            return $http.delete(url+'/'+id)
        }
    };
}]);