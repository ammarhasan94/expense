/**
 * Created by ammar on 9/8/15.
 */


var routerApp = angular.module('myApp', ['ui.router']);

routerApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/head');

    $stateProvider

        .state('head', {
            url: '/head',
            templateUrl: 'partial/head.html',
            controller:'HeadCtrl'
        })

        .state('expense', {
            url: '/expense',
            templateUrl: 'partial/expense.html',
            controller:'ExpenseCtrl'
        })
        .state('expense.create', {
            url: '/create',
            templateUrl: 'partial/expense_create.html'
        })
        .state('reports', {
            url: '/reports',
            templateUrl: 'partial/reports.html',
            controller:'ReportsCtrl'
        });

});