/**
 * Created by ammar on 9/8/15.
 */
routerApp.controller('ExpenseCtrl',function($scope,Expense,Head){

    Head.getHeads().success(function(data){ $scope.heads=data;});
    Expense.getExpenses().success(function(data){ $scope.expenses=data;});

    clearTemporaryObjects();

    function clearTemporaryObjects() {
        $scope.newExpense = {amount: 0, detail: '',_head:''};
    }


    $scope.addExpense=function(){
        Expense.postExpense($scope.newExpense).success(function(data){

            $scope.expenses.push($scope.newExpense);
            console.log('Data: '+$scope.newExpense);
            console.log('Expenses: '+$scope.expenses);
        });
        clearTemporaryObjects();
    };
    //$scope.editHead=function(index){
    //    $scope.heads[index].isEditing = true;
    //};
    //
    //$scope.saveHead=function(index){
    //    $scope.heads[index].isEditing = false;
    //    Head.updateHead($scope.heads[index]);
    //};
    //
    //
    //$scope.deleteHead=function(index){
    //    Head.deleteHead($scope.heads[index]._id);
    //    $scope.heads.splice(index,1);
    //}

});
