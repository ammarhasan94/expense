/**
 * Created by ammar on 9/8/15.
 */
routerApp.controller('HeadCtrl',function($scope,Head){
    Head.getHeads().success(function(data){ $scope.heads=data;});

    $scope.newHead='';
    $scope.isEditing=false;

    $scope.editHead=function(index){
        $scope.heads[index].isEditing = true;
    };

    $scope.saveHead=function(index){
        $scope.heads[index].isEditing = false;
        Head.updateHead($scope.heads[index]);
    };

    $scope.addHead=function(){
        Head.addHead({name:$scope.newHead}).success(function(data){ $scope.heads.push(data)});
        $scope.newHead='';
    };

    $scope.deleteHead=function(index){
        Head.deleteHead($scope.heads[index]._id);
        $scope.heads.splice(index,1);
    }


});
