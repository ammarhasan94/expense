/**
 * Created by ammar on 9/7/15.
 */
var http=require('http');
var express = require('express');
var app=express();
var bodyParser = require('body-parser');
var passport=require('passport');
var path = require('path');

//Database
var db=require('./db.js');

var users = require('./routes/users');
var expense=require('./routes/expense');
var expenseHead=require('./routes/expense_head');

//config
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

//routes
app.use('/users', users);
app.use('/expense',expense);
app.use('/head',expenseHead);

var listener=app.listen(3000);
console.log('server Listening on port ',listener.address().port);


