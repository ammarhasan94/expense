/**
 * Created by ammar on 9/7/15.
 */
var express = require('express');
var router = express.Router();
var ExpenseHead = require('../models/Expense').ExpenseHead;

//GET: All expense heads, with their expense
router.get('/', function (req, res) {
    ExpenseHead.find({}, function (err, data) {
        if (err)
            res.json(err);
        else
            res.json(data);
    });

});
//GET: A expense head, with its expense
router.get('/:id', function (req, res) {
    ExpenseHead.find({_id: req.params.id}, function (err, data) {
        if (err)
            res.json(err);
        else
            res.json(data);
    });

});

//POST: A expense head
router.post('/', function (req, res) {
    var expenseHead = new ExpenseHead({
        name: req.body.name,
        expense: []
    });
    expenseHead.save(function (err, expenseHead) {
        if (err) return console.error(err);
        else res.json(expenseHead);
    });


});

// PUT: Edit a particular head
router.put('/', function (req, res) {
    ExpenseHead.findOneAndUpdate(
        {_id: req.body._id},
        {
            $set: {name: req.body.name}
        },
        function (err, data) {
            if (err) {
                console.error(err);
                return res.status(500);

            }
            else if(data) {
                res.status(200);
                res.json({success: true});
            }
            else{
                res.status(404);
                res.json('not found');
            }
        });
});


// DELETE: Edit a particular ExpenseHead
router.delete('/:id', function (req, res) {
    console.log(req.params.id);
    ExpenseHead.findOneAndRemove({_id: req.params.id},function(err,data){
        if(err){
            console.error(err);
            res.status(500);
            return res.json({success:false});
        }
        else if(data){
            return res.json({success:true});
        }
        else{
            res.status(404);
            res.json('Not found');
        }
    });
});
module.exports = router;