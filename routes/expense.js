/**
 * Created by ammar on 9/7/15.
 */
var express=require('express');
var router=express.Router();
var Expense=require('../models/Expense').Expense;
var ExpenseHead = require('../models/Expense').ExpenseHead;



// GET: Retrieve All expenses
router.get('/',function(req,res){
    Expense.find({}).populate('_head')
        .exec(function (err, data) {
            console.log(data);
            if (err) return res.status(500);
            else res.json(data);
        });
});

// GET: Retrieve wrt Id
router.get('/:id',function(req,res){
    Expense.find({_id:req.params.id},function(err,data){
        if(err){
            return res.status(500);
        }
        else{
            res.status(200)
            res.json(data);
        }
    });
});

// POST: Add a expense in a Expense head
router.post('/',function(req,res){

    var expense = new Expense({
        _head: req.body._head,
        amount: req.body.amount,
        detail:req.body.detail,
        date:req.body.date
    });
    expense.save(function(err,expense){
        if(err){
            console.error(err);
            return res.status(500);

        }
        else{
            res.status(201);
            res.json({success:true});
        }
    });

    ExpenseHead.findOne({_id: req.body._head}, function (err, data) {
        data.expense.push(expense);
        data.save(function(err,data){
            if(err){
                console.log({success:false});
            }
            else{
                console.log({success:true});
            }
        });

    });
});

// PUT: Edit a particular expense
router.put('/', function (req, res) {
    Expense.findOneAndUpdate(
        {_id: req.body.id},
        {
            $set: {
                amount: req.body.amount,
                detail:req.body.detail,
                date:req.body.date
            }
        },
        function (err, data) {
            if (err) {
                console.error(err);
                return res.status(500);

            }
            else if(data){
                res.status(200);
                res.json({success: true});
            }
            else{
                res.status(404);
                res.json('Not Found');
            }
        });
});

// DELETE: Edit a particular expense
router.delete('/:id', function (req, res) {
    Expense.findOneAndRemove({_id: req.params.id},function(err){
        if(err){
            console.error(err);
            return res.json({success:false});
        }
        else{
            return res.json({success:true});
        }
    });
});

 module.exports=router;