/**
 * Created by  on 9/7/15.
 */

var mongoose=require('mongoose');

var UserSchema=new mongoose.Schema({
    name: String,
    email:String,
    userName: String,
    password: String
});

var User=mongoose.model('User',UserSchema)

module.exports=User;