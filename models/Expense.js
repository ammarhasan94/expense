/**
 * Created by ammar on 9/7/15.
 */

var mongoose = require('mongoose');

var HeadSchema = new mongoose.Schema({
//    _id:mongoose.Schema.Types.ObjectId,
    name: String,
    expense: [{type: mongoose.Schema.Types.ObjectId, ref: 'Expense'}]
});

var ExpenseSchema = new mongoose.Schema({
    _head: {type: mongoose.Schema.Types.ObjectId, ref: 'ExpenseHead'},
    amount: Number,
    date: {type: Date, default: Date.now()},
    detail: String
});

var Expense = mongoose.model('Expense', ExpenseSchema);
var ExpenseHead = mongoose.model('ExpenseHead', HeadSchema);


module.exports = {
    ExpenseHead: ExpenseHead,
    Expense: Expense
};